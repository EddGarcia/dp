# README #
* Autor:   Eduard Kubinec
* Skola:   FIIT STU
* Projekt: Diplomova praca
* Nazov:   Použitie biznis analýz na zefektívnenie procesov v podnikoch

# TEMA #
## Hyperparametricka optimalizacia SVR ##
Optimalizacia parametrov SVR pomocou metaheuristickych pristupov. Ich porovnanie z hladiska konvergencie a presnosti. Modely ziskane pomocou parametrov z metaheuristik su testovane pri predikcii casoveho radu.

# OBSAH #
1. Skripty v jazyku R pre predikciu a optimalizacie
2. Java project s naimplementovanou metodou GWO - grey wolf optimizer
3. Dokument k praci

# DONE #
* Porovnanie kernelov z hladiska presnosti aproximacie funkcie na problem casoveho radu (gausian, polynomial, sigmoid)
* optimalna metoda Grid search
* metaheuristika GA
* metaheuristika PSO
* metaheuristika DE
* java aplikacia s optimalizacnou metodou GWO
* komponent pre vizualizaciu a spracovavanie casoveho radu Java
#---------------------------------------------------------------------
# Vytvor dummy sinus data pre dany pocet dni 
# s danou periodou
# Issue: pre velky pocet dni nepresne lebo vypocty nie su uplne presne
# --------------------------------------------------------------------


makeSinDummyData <- function(numDays, measurmentInDay){
  temp <- seq(0,(pi*2)*numDays,(pi*2)/measurmentInDay)
  sinData <- sin(temp[1:length(temp)-1])

  return(as.vector(sinData))
}
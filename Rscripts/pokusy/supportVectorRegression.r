library(kernlab)
library(GA)

#-------------------Datove funkcie---------------------
# Vytvor dummy sinus data
makeDummyData <- function(numDays, measurmentInDay){
  temp <- seq(0,(pi*2)*numDays,(pi*2)/measurmentInDay)
  sinData <- sin(temp[1:length(temp)-1])
  return(as.vector(sinData))
}

#--------------------Helpery---------------------------

rmseFunction <- function(vector1, vector2){
  length1 <- length(as.vector(vector1))
  length2 <- length(as.vector(vector2))
  
  if(length1 != length2){
    return(-1)
  }
  
  rmse <- mean((vector1 - vector2) ^ 2)
  return(rmse)
}

computeSimpleError <- function(vector1, vector2){
  length1 <- length(as.vector(vector1))
  length2 <- length(as.vector(vector2))
  
  if(length1 != length2){
    return(-1)
  }
  
  totalerror <- 0
  for(i in 1:length1){
    error <- vector1[i]-vector2[i]
    totalerror <- totalerror + abs(error)
  }
  
  averageerror <- totalerror/length1
  return(averageerror)
}

#-------------------SVR funkcie------------------------

# Vstupom su hodnoty casoveho radu za X dni - trainset a parametre SVR
predictionSVR <- function(trainSet, c, gamma, epsilon){
  
  # Zistim velkost vektora - vektor obsahuje hodnoti merani v intervaloch 15 minut cize 15*10 dni napr.
  N <- length(as.vector(trainSet))
  
  # Vytvorim nulovu maticu s 97 stlpcami a N riadkami -  N je velkost vektora
  matica <- matrix(0, nrow = N, ncol = 97)
  
  # Do matice nasukam jednotky na pozicie odberov
  for(j in 1:96){
    matica[seq(j, N, by = 96), j+1] <- 1
  } 
  
  # Do prveho stlpca dam namerane hodnoty
  matica[,1] <- trainSet
  
  # Inicializacia labelov 
  param <- vector(length = 96)
  for(i in 1:96){
    param[i] <- paste("V", i, sep = "")
  }
  
  # Pomenovanie stlpcov labelmi
  colnames(matica) <- c("Load", param)
  
  #print(epsilon)
  # Budem predikovat Load na zaklade ostatnych parametrov, pomocou dat matica
  svr <- ksvm(Load ~ V1 + V2 + V3 + V4 + V5 + V6 + V7 + V8 + V9 + V10 + V11 + V12 + V13 + V14 + V15 + V16 + V17 +
                V18 + V19 + V20 + V21 + V22 + V23 + V24 + V25 + V26 + V27 + V28 + V29 + V30 + V31 + V32 + V33 + 
                V34 + V35 + V36 + V37 + V38 + V39 + V40 + V41 + V42 + V43 + V44 + V45 + V46 + V47 + V48 + V49 +
                V50 + V51 + V52 + V53 + V54 + V55 + V56 + V57 + V58 + V59 + V60 + V61 + V62 + V63 + V64 + V65 + 
                V66 + V67 + V68 + V69 + V70 + V71 + V72 + V73 + V74 + V75 + V76 + V77 + V78 + V79 + V80 + V81 + 
                V82 + V83 + V84 + V85 + V86 + V87 + V88 + V89 + V90 + V91 + V92 + V93 + V94 + V95 + V96,
              data = matica, type = "eps-svr", kernel = "rbfdot", kpar=list(sigma=gamma), eps = epsilon, C = c)
  
  # Predikcia pomocou svr - a predikujem hodnotu do buducnosti
  svr.pred <- predict(svr, matica[1:96,-1])
  
  return(as.vector(svr.pred))
}

#-------------------GA funkcie-------------------------

evaluateParams <- function(trainSet, testSet, c, gamma, epsilon){
  #Trenovanie a predikcia
  prediction <- predictionSVR(trainSet, c, gamma, epsilon) 
    
  #Vyhodnotenie
  rmseOfSolution <- rmseFunction(prediction, testSet)
  
  return(rmseOfSolution)
}

# Do funkcie vstupuje vektor hodnot parametrov a dataset ktory ma strukturu listu listov ktory obsahuje trenovacie a testovacie data
getFitnessOfSolution <- function(x, dataSet){
  c <- x[1]
  gamma <- x[2]
  epsilon <- x[3]
  
  # Vypocitaj rmse pre kazdy set trenovacich a testovacich dat - foldData je jedna sada dat a teda trenovacia + testovacia mnozina
  rmseVector <- sapply(dataSet, 
                      function(foldData) with(foldData, 
                      evaluateParams(train_data, test_data, c, gamma, epsilon)))
  
  # Vratim priemer z RMSE vektora ale zaporny pretoze rmse chcem minimalizovat zatial co fitness je maximalizovana v GA
  return (-mean(rmseVector))
}

# Ako parameter vstupuje list listov [[1(testData,trainData)],[2((testData,trainData)],[3((testData,trainData)],...]
tuneSVRUsingGA <- function(dataSet){
  
  # Urcime rozsahy parametrov vo forme v akej ju potrebuje ga algoritmus
  minValues <- c(c = 1e-4, gamma = 1e-3, epsilon = 1e-2)
  maxValues <- c(c = 10, gamma = 2, epsilon = 1)
  
  # Spustenie genetickeho algoritmu
  results <- ga(type = "real-valued", 
                fitness = getFitnessOfSolution, 
                dataSet, 
                names = names(minValues), 
                elitism = 0.01,
                min = minValues, 
                max = maxValues,
                popSize = 10, 
                maxiter = 10
                )
  
  return(results)
}
#-----------------------------------------------------

#--------------------BEGIN Program--------------------
# Pocet foldov pri krosvalidacii
K = 1

# Dataset skladajuci sa z listov kazdy obsahujuci trenovacie(3dni) a testovacie(1den) data
dataSet <- lapply(1:K, function(i) list(
  train_data = makeDummyData(3,96),   
  test_data  = makeDummyData(1,96))
  )

result <- tuneSVRUsingGA(dataSet)
summary(result)

finalDataSet = list(
  train_data = makeDummyData(3,96),
  test_data  = makeDummyData(1,96)
  )

result@solution[1]
result@solution[2]
result@solution[3]

predictionVector = predictionSVR(finalDataSet$train_data, result@solution[1], result@solution[2], result@solution[3])

plot(finalDataSet$test_data, pch=20, xlab="x", ylab="sin(x)")
lines(finalDataSet$test_data, col = "green", pch=4)
lines(predictionVector, col = "red", pch=4)


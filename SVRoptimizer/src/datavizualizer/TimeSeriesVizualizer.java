package datavizualizer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import utils.LabeledTimeSeries;

@SuppressWarnings("serial")
public class TimeSeriesVizualizer extends ApplicationFrame {
	
	public TimeSeriesVizualizer(final LabeledTimeSeries ts) {
		super(ts.getTitle());

		final JFreeChart tschart = createChart(ts);
		
		final ChartPanel chartPanel = new ChartPanel(tschart);
		chartPanel.setPreferredSize(new java.awt.Dimension(560, 370));
		chartPanel.setMouseZoomable(true, false);
		setContentPane(chartPanel);
	}

	private JFreeChart createChart(final LabeledTimeSeries timeseries) {
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(timeseries.getTimeseries());
		
		XYSeries series1 = new XYSeries("Object 1");
		 
	    series1.add(1.0, 212.0);
	    series1.add(2.0, 300.0);
	    series1.add(3.0, 2785.5);
	    series1.add(4.0, 245.8);
	    series1.add(5.0, 674.0);
		
	    dataset.addSeries(series1);
	    
		String description = timeseries.getTitle();
		String labelX = timeseries.getLabelX();
		String labelY = timeseries.getLabelY();

		return ChartFactory.createTimeSeriesChart(description, labelX, labelY,
				dataset, false, false, false);
	}

//	   public static void main( final String[ ] args )
//	   {
//	      final String title = "Time Series Management";         
//	      final TimeSeriesVizualizer demo = new TimeSeriesVizualizer( title );         
//	      demo.pack( );         
//	      RefineryUtilities.positionFrameRandomly( demo );         
//	      demo.setVisible( true );
//	   }	
}
   
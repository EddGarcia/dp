package experimentation;

import gwo.core.Bounds;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import loader.CSVTimeSeriesDataLoader;
import main.Config;
import main.Controller;
import optimizer.methods.GridSearch;
import optimizer.paramstructs.DEparamStruct;
import optimizer.paramstructs.GAparamStruct;
import optimizer.paramstructs.GWOparamStruct;
import optimizer.paramstructs.PSOparamStruct;
import optimizer.utils.OptimMethod;
import optimizer.utils.Result;

public class Experiment {
	public static void main(String[] args){
		CSVTimeSeriesDataLoader loader = new CSVTimeSeriesDataLoader();

		try{
			/*LabeledTimeSeries ts = loader.LoadData("data/milkData2.txt","Mlieko");
			TimeSeriesVizualizer vizualizer = new TimeSeriesVizualizer(ts);
			
	        vizualizer.pack();         
	        RefineryUtilities.positionFrameRandomly(vizualizer);        
	        vizualizer.setVisible(true);
	      
			//GridSearch gs = new GridSearch();
			//gs.makeDataFileFromGridsearch();
			
			/*SliceConverter slice = new SliceConverter();
			slice.convertToSlice(0.5 ,"gridSearchData.txt");*/
			
//			PSO pso = new PSO();
//			pso.optimizeParameters();
//			pso.optimizeParameters();
//			
//			PSO pso2 = new PSO();
//			pso2.optimizeParameters();
			//MainWindow win = new MainWindow();
			
			Controller c = new Controller();
			//c.initMainWindow();
			c.loadRequiredRScripts();
			c.loadCsvData(Config.pathToCSVData);
			Experiment ex = new Experiment();
			
			ex.makeGWOExperiment(c, 50);
			ex.makePSOExperiment(c, 50);
			ex.makeDEExperiment(c, 50);
			ex.makeGAExperiment(c, 50);
			//ex.makeGridSearchExperiment(c);
			
			//System.out.println("DE>  "+ex.getAverageError("DE_optimResult_20160503_142654.txt"));
			//System.out.println("GA>  "+ex.getAverageError("GA_optimResult_20160504_003142.txt"));
			//System.out.println("GWO> "+ex.getAverageError("GWO_optimResult_20160503_042920.txt"));
			//System.out.println("PSO> "+ex.getAverageError("PSO_optimResult_20160503_094323.txt"));
			
			//SliceConverter s = new SliceConverter();
			//s.convertToSlice(0.12001, "gridSearchData.txt");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void makeGridSearchExperiment(Controller c){
		GridSearch gs = new GridSearch();
		try{
			gs.makeDataFileFromGridsearch();
		}
		catch(Exception e){
			System.out.println("Grid search chyba");
			e.printStackTrace();
		}
	}
	
	private void makeGAExperiment(Controller c, int noTimes){
		List<Result> results = new ArrayList<Result>();
		
		GAparamStruct params = new GAparamStruct();
		Bounds bounds = new Bounds();
		
		bounds.setLowerBounds(new double[] {Config.epsilonMinValue,Config.costMinValue,Config.gammaMinValue});
		bounds.setUpperBounds(new double[] {Config.epsilonMaxValue,Config.costMaxValue,Config.gammaMaxValue});
		params.setBounds(bounds);
		params.setMaxIterations(Config.GAmaxIterations);
		params.setPopSize(Config.GApopSize);
		params.setElitism(Config.GAElitism);
		params.setpCrossOver(Config.GACrossOver);
		params.setpMutation(Config.GAMutation);
		
		params.setNoOfFolds(Config.noOfFolds);
		
		for(int i=0; i<noTimes; i++){
			results.add(c.dispatchOptimizeCall(OptimMethod.GA, params));
			System.out.println("Result added> "+(i+1)+"/"+noTimes);
		}
		
		try{
			this.printResultsToFile(results,OptimMethod.GA);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void makePSOExperiment(Controller c, int noTimes){
		List<Result> results = new ArrayList<Result>();
		
		PSOparamStruct params = new PSOparamStruct();
		Bounds bounds = new Bounds();
		
		bounds.setLowerBounds(new double[] {Config.epsilonMinValue,Config.costMinValue,Config.gammaMinValue});
		bounds.setUpperBounds(new double[] {Config.epsilonMaxValue,Config.costMaxValue,Config.gammaMaxValue});
		params.setBounds(bounds);
		params.setMaxIterations(Config.PSOmaxIterations);
		params.setSwarmSize(Config.PSOswarmSize);
		params.setNoOfFolds(Config.noOfFolds);
		
		for(int i=0; i<noTimes; i++){
			results.add(c.dispatchOptimizeCall(OptimMethod.PSO, params));
			System.out.println("Result added> "+(i+1)+"/"+noTimes);
		}
		
		try{
			this.printResultsToFile(results,OptimMethod.PSO);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void makeDEExperiment(Controller c, int noTimes){
		List<Result> results = new ArrayList<Result>();
		
		DEparamStruct params = new DEparamStruct();
		Bounds bounds = new Bounds();
		
		bounds.setLowerBounds(new double[] {Config.epsilonMinValue,Config.costMinValue,Config.gammaMinValue});
		bounds.setUpperBounds(new double[] {Config.epsilonMaxValue,Config.costMaxValue,Config.gammaMaxValue});
		params.setBounds(bounds);
		params.setNoOfFolds(Config.noOfFolds);
		params.setMaxIterations(Config.DEmaxIterations);
		params.setPopSize(Config.DEpopSize);
		params.setCoAdaptationSpeed(Config.DEcoAdaptationSpeed);
		params.setCrossOverProb(Config.DEcrossOverProb);
		params.setDifferetialWightFactor(Config.DEdifferetialWightFactor);
		params.setStrategy(2);
		
		for(int i=0; i<noTimes; i++){
			results.add(c.dispatchOptimizeCall(OptimMethod.DE, params));
			System.out.println("Result added> "+(i+1)+"/"+noTimes);
		}
		
		try{
			this.printResultsToFile(results,OptimMethod.DE);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void makeGWOExperiment(Controller c, int noTimes){
		List<Result> results = new ArrayList<Result>();
		
		GWOparamStruct params = new GWOparamStruct();
		Bounds bounds = new Bounds();
		
		bounds.setLowerBounds(new double[] {Config.epsilonMinValue,Config.costMinValue,Config.gammaMinValue});
		bounds.setUpperBounds(new double[] {Config.epsilonMaxValue,Config.costMaxValue,Config.gammaMaxValue});
		params.setBounds(bounds);
		params.setMaxIterations(Config.GWOmaxIterations);
		params.setWolfPackSize(Config.GWOpackSize);
		params.setNoOfFolds(Config.noOfFolds);
		
		for(int i=0; i<noTimes; i++){
			results.add(c.dispatchOptimizeCall(OptimMethod.GWO, params));
			System.out.println("Result added> "+(i+1)+"/"+noTimes);
		}
		
		try{
			this.printResultsToFile(results,OptimMethod.GWO);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void printResultsToFile(List<Result> results, OptimMethod method) throws Exception{
		PrintWriter writer = new PrintWriter(method.name()+"_optimResult_"+new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())+".txt");
		PrintWriter writer2 = new PrintWriter(method.name()+"_convInfo_"+new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime())+".txt");
		
		int length = results.get(0).getOptimResult().getConvergenceInfo().length;
		double conv[] = new double[length];
		Arrays.fill(conv, 0);
		
		for(Result r : results){
			writer.println(r.toString());
			
			double[] currentConv = r.getOptimResult().getConvergenceInfo();
			for(int i=0; i< length; i++){
				conv[i] += currentConv[i];
			}
		}
		
		for(int i=0; i<length; i++){
			conv[i] = conv[i]/results.size();
			writer2.print(conv[i]+",");
		}
		
		writer.close();
		writer2.close();
	}
	
	private double getAverageError(String filePath){
		try {
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			
			String line;
			int counter = 0;
			double sum = 0;
			while((line = br.readLine()) != null){
				String[] tokens = line.split(" ");
				sum += Double.parseDouble(tokens[tokens.length-2]);
				counter++;
			}
			return sum/(double)counter;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return -1;
	}
}

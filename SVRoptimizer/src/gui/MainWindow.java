package gui;

import gwo.core.Bounds;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutionException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import main.Config;
import main.Controller;
import optimizer.paramstructs.DEparamStruct;
import optimizer.paramstructs.GAparamStruct;
import optimizer.paramstructs.GWOparamStruct;
import optimizer.paramstructs.PSOparamStruct;
import optimizer.paramstructs.ParamStruct;
import optimizer.utils.OptimMethod;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	private Controller mainController;
	private JDialog loadingDialog;
	
	private JPanel boundsPanel;
	private JPanel psoPanel;
	private JPanel gwoPanel;
	private JPanel dePanel;
	private JPanel gaPanel;
	
	private JButton psoButton;
	private JButton gwoButton;
	private JButton deButton;
	private JButton gaButton;

	//Bounds panel
	private JTextField minEps;
	private JTextField maxEps;
	private JTextField minCost;
	private JTextField maxCost;
	private JTextField minGamma;
	private JTextField maxGamma;
	private JTextField validationFoldsNo;
	
	private JLabel minEpsL;
	private JLabel maxEpsL;
	private JLabel minCostL;
	private JLabel maxCostL;
	private JLabel minGammaL;
	private JLabel maxGammaL;
	private JLabel validationFoldsNoL;
	private JPanel unitEps;
	private JPanel unitCost;
	private JPanel unitGamma;
	
	//PSO panel
	private JTextField PSOswarmSize;
	private JTextField PSOmaxIterations;
	private JLabel PSOswarmSizeL;
	private JLabel PSOmaxIterationsL;
	private JPanel PSObutPanel;
	
	//GWO panel
	private JTextField GWOpackSize;
	private JTextField GWOmaxIterations;
	private JLabel GWOpackSizeL;
	private JLabel GWOmaxIterationsL;
	private JPanel GWObutPanel;
	
	//DE panel
	private JTextField DEmaxIterations;
	private JTextField DEpopSize;
	private JTextField DEcrossOverProb;
	private JTextField DEdifferentialWeight;
	private JTextField DEcrossOverAdaptation;
	private JPanel DEbutPanel;
	
	private JLabel DEmaxIterationsL;
	private JLabel DEpopSizeL;
	private JLabel DEcrossOverProbL;
	private JLabel DEdifferentialWeightL;
	private JLabel DEcrossOverAdaptationL;
	private JLabel DEStrategyLabel;
	
	private ButtonGroup DEstrategyGroup;
	private JRadioButton DEstrat1;
	private JRadioButton DEstrat2;
	private JRadioButton DEstrat3;
	
	private JPanel deUnit1;
	private JPanel deUnit2;
	
	//GA panel
	private JTextField GAmaxIterations;
	private JTextField GApopSize;
	private JTextField GAcrossOverProb;
	private JTextField GAmutProb;
	private JTextField GAelitism;
	private JPanel GAbutPanel;
	private JPanel GASettingsPanel;
	private JPanel GAUnit1;
	private JPanel GAUnit2;
	
	private JLabel GAmaxIterationsL;
	private JLabel GApopSizeL;
	private JLabel GAcrossOverProbL;
	private JLabel GAmutProbL;
	private JLabel GAelitismL;
	
	
	public MainWindow(Controller c){
		super("Meta optimizer");
		this.mainController = c;
		
		this.setSize(600,760);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setIconImage(new ImageIcon(Config.iconPath).getImage());
		this.setLayout(new FlowLayout());
		//this.getContentPane().setBackground(Color.GRAY);
		
		this.initializeOptimPanels();
	}
	
	private void initializePSOpanel(){
		PSOswarmSize = new JTextField(5);
		PSOswarmSize.setText(String.valueOf(Config.PSOswarmSize));
		
		PSOmaxIterations = new JTextField(5);
		PSOmaxIterations.setText(String.valueOf(Config.PSOmaxIterations));
		
		PSOswarmSizeL = new JLabel("Swarm size    ");
		PSOmaxIterationsL = new JLabel("Maximum iterations");
		
		this.psoPanel.add(PSOswarmSizeL);
		this.psoPanel.add(PSOswarmSize);
		this.psoPanel.add(PSOmaxIterationsL);
		this.psoPanel.add(PSOmaxIterations);
	}
	
	private void initializeGWOpanel(){
		GWOpackSize = new JTextField(5);
		GWOpackSize.setText(String.valueOf(Config.GWOpackSize));
		
		GWOmaxIterations = new JTextField(5);
		GWOmaxIterations.setText(String.valueOf(Config.PSOmaxIterations));
		
		GWOpackSizeL = new JLabel("Wolfpack size");
		GWOmaxIterationsL = new JLabel("Maximum iterations");
		
		this.gwoPanel.add(GWOpackSizeL);
		this.gwoPanel.add(GWOpackSize);
		this.gwoPanel.add(GWOmaxIterationsL);
		this.gwoPanel.add(GWOmaxIterations);
	}
	
	private void initializeGApanel(){
		GApopSize = new JTextField(5);
		GApopSize.setText(String.valueOf(Config.GApopSize));
		
		GAmaxIterations = new JTextField(5);
		GAmaxIterations.setText(String.valueOf(Config.GAmaxIterations));
		
		GAcrossOverProb = new JTextField(5);
		GAcrossOverProb.setText(String.valueOf(Config.GACrossOver));
		
		GAmutProb = new JTextField(5);
		GAmutProb.setText(String.valueOf(Config.GAMutation));
		
		GAelitism = new JTextField(5);
		GAelitism.setText(String.valueOf(Config.GAElitism));
		
		GASettingsPanel = new JPanel();
		GASettingsPanel.setPreferredSize(new Dimension(300,200)); 
		
		GAUnit1 = new JPanel();
		GAUnit1.setPreferredSize(new Dimension(260,80));
		
		GAUnit2 = new JPanel();
		GAUnit2.setPreferredSize(new Dimension(260,80));
		
		GApopSizeL = 				new JLabel("Population size         ");
		GAmaxIterationsL = 			new JLabel("Maximum iterations");
		GAcrossOverProbL = 			new JLabel("Crossover probability <0,1>");
		GAmutProbL = 				new JLabel("Mutation probability    <0,1>");
		GAelitismL = 				new JLabel("Elitism                                       ");
		
		GAUnit1.add(GApopSizeL);
		GAUnit1.add(GApopSize);
		GAUnit1.add(GAmaxIterationsL);
		GAUnit1.add(GAmaxIterations);
		
		GAUnit2.add(GAmutProbL);
		GAUnit2.add(GAmutProb);
		GAUnit2.add(GAcrossOverProbL);
		GAUnit2.add(GAcrossOverProb);
		GAUnit2.add(GAelitismL);
		GAUnit2.add(GAelitism);
		
		gaPanel.add(GAUnit1);
		gaPanel.add(GAUnit2);		
	}
	
	private void initializeDEpanel(){
		DEpopSize = new JTextField(5);
		DEpopSize.setText(String.valueOf(Config.DEpopSize));
		
		DEmaxIterations = new JTextField(5);
		DEmaxIterations.setText(String.valueOf(Config.DEmaxIterations));
		
		DEcrossOverProb = new JTextField(5);
		DEcrossOverProb.setText(String.valueOf(Config.DEcrossOverProb));
		
		DEcrossOverAdaptation = new JTextField(5);
		DEcrossOverAdaptation.setText(String.valueOf(Config.DEcoAdaptationSpeed));
		
		DEdifferentialWeight = new JTextField(5);
		DEdifferentialWeight.setText(String.valueOf(Config.DEdifferetialWightFactor));
		
		DEpopSizeL = 				new JLabel("Population size                                    ");
		DEmaxIterationsL = 			new JLabel("Maximum iterations                           ");
		DEcrossOverAdaptationL = 	new JLabel("Crossover adaptation speed <0,1>");
		DEcrossOverProbL = 			new JLabel("Crossover probability <0,1>             ");
		DEdifferentialWeightL = 	new JLabel("Differential weight factor <0,2>       ");
		
		DEstrategyGroup = new ButtonGroup();
		DEstrat1 = new JRadioButton("Rand              ",false);
		DEstrat2 = new JRadioButton("LocalToBest",true);
		DEstrat3 = new JRadioButton("Best               ",false);
		DEstrategyGroup.add(DEstrat1);
		DEstrategyGroup.add(DEstrat2);
		DEstrategyGroup.add(DEstrat3);
		DEStrategyLabel = new JLabel("Strategy         ");
		
		deUnit1 = new JPanel();
		deUnit1.setPreferredSize(new Dimension(270,130));
		
		deUnit2 = new JPanel();
		deUnit2.setPreferredSize(new Dimension(100,130));
		
		deUnit1.add(DEpopSizeL);
		deUnit1.add(DEpopSize);
		deUnit1.add(DEmaxIterationsL);
		deUnit1.add(DEmaxIterations);
		deUnit1.add(DEcrossOverProbL);
		deUnit1.add(DEcrossOverProb);
		deUnit1.add(DEcrossOverAdaptationL);
		deUnit1.add(DEcrossOverAdaptation);
		deUnit1.add(DEdifferentialWeightL);
		deUnit1.add(DEdifferentialWeight);
		
		deUnit2.add(DEStrategyLabel);
		deUnit2.add(DEstrat1);
		deUnit2.add(DEstrat2);
		deUnit2.add(DEstrat3);
		
		this.dePanel.add(deUnit1);
		this.dePanel.add(deUnit2);
	}
	
	private void initializeBoundsPanel(){
		minEps = new JTextField(5);
		minEps.setText(String.valueOf(Config.epsilonMinValue));
		maxEps = new JTextField(5);
		maxEps.setText(String.valueOf(Config.epsilonMaxValue));
		
		minCost = new JTextField(5);
		minCost.setText(String.valueOf(Config.costMinValue));
		maxCost = new JTextField(5);
		maxCost.setText(String.valueOf(Config.costMaxValue));
		
		minGamma = new JTextField(5);
		minGamma.setText(String.valueOf(Config.gammaMinValue));
		maxGamma = new JTextField(5);
		maxGamma.setText(String.valueOf(Config.gammaMaxValue));
		
		validationFoldsNo = new JTextField(5);
		validationFoldsNo.setText(String.valueOf(Config.noOfFolds));
		
		minEpsL = new JLabel("Epsilon min ");
		maxEpsL = new JLabel("Epsilon max");
		minCostL = new JLabel("Cost min ");
		maxCostL = new JLabel("Cost max");
		minGammaL = new JLabel("Gamma min ");
		maxGammaL = new JLabel("Gamma max");
		validationFoldsNoL = new JLabel("Number of folds in K-fold cross validation");
		
		unitEps = new JPanel();
		unitEps.setPreferredSize(new Dimension(170,60));
		unitCost = new JPanel();
		unitCost.setPreferredSize(new Dimension(170,60));
		unitGamma = new JPanel();
		unitGamma.setPreferredSize(new Dimension(170,60));

		this.unitEps.add(minEpsL);
		this.unitEps.add(minEps);
		this.unitEps.add(maxEpsL);
		this.unitEps.add(maxEps);
		this.boundsPanel.add(unitEps);
		
		this.unitCost.add(minCostL);
		this.unitCost.add(minCost);
		this.unitCost.add(maxCostL);
		this.unitCost.add(maxCost);
		this.boundsPanel.add(unitCost);
		
		this.unitGamma.add(minGammaL);
		this.unitGamma.add(minGamma);
		this.unitGamma.add(maxGammaL);
		this.unitGamma.add(maxGamma);
		this.boundsPanel.add(unitGamma);
		
		this.boundsPanel.add(validationFoldsNoL);
		this.boundsPanel.add(validationFoldsNo);
	}
	
	private void initializeOptimPanels(){
		boundsPanel = new JPanel();
		boundsPanel.setPreferredSize(new Dimension(570,120));
		boundsPanel.setBorder(BorderFactory.createTitledBorder("Bounds of search space"));
		boundsPanel.setBackground(Color.LIGHT_GRAY);
		this.initializeBoundsPanel();
		
		psoPanel = new JPanel();
		psoPanel.setPreferredSize(new Dimension(570,100));
		psoPanel.setBorder(BorderFactory.createTitledBorder("PSO - Particle Swarm Optimization"));
		this.initializePSOpanel();
		
		gwoPanel = new JPanel();
		gwoPanel.setPreferredSize(new Dimension(570,100));
		gwoPanel.setBorder(BorderFactory.createTitledBorder("GWO - Grey Wolf Optimization"));
		this.initializeGWOpanel();
		
		dePanel = new JPanel();
		dePanel.setPreferredSize(new Dimension(570,210));
		dePanel.setBorder(BorderFactory.createTitledBorder("DE - Differential evolution"));
		this.initializeDEpanel();
		
		gaPanel = new JPanel();
		gaPanel.setPreferredSize(new Dimension(570,160));
		gaPanel.setBorder(BorderFactory.createTitledBorder("GA - Genetic algorithm"));
		this.initializeGApanel();
		
		psoButton = new JButton("PSO optim");
		psoButton.addActionListener(new OptimizeButonActionListener());
		PSObutPanel = new JPanel();
		PSObutPanel.setPreferredSize(new Dimension(300,40));
		
		gwoButton = new JButton("GWO optim");
		gwoButton.addActionListener(new OptimizeButonActionListener());
		GWObutPanel = new JPanel();
		GWObutPanel.setPreferredSize(new Dimension(300,40));
		
		deButton = new JButton("DE  optim");
		deButton.addActionListener(new OptimizeButonActionListener());
		DEbutPanel = new JPanel();
		DEbutPanel.setPreferredSize(new Dimension(300,40));
		
		gaButton = new JButton("GA  optim");
		gaButton.addActionListener(new OptimizeButonActionListener());
		GAbutPanel = new JPanel();
		GAbutPanel.setPreferredSize(new Dimension(300,40));
		
		PSObutPanel.add(psoButton);
		GWObutPanel.add(gwoButton);
		DEbutPanel.add(deButton);
		GAbutPanel.add(gaButton);
		
		psoPanel.add(PSObutPanel);
		gwoPanel.add(GWObutPanel);
		dePanel.add(DEbutPanel);
		gaPanel.add(GAbutPanel);
		
		this.add(boundsPanel);
		this.add(psoPanel);
		this.add(gwoPanel);
		this.add(dePanel);
		this.add(gaPanel);
	}
	
	private void initializeDialog(JDialog loadingDialog, JFrame parent) {
		JLabel gif = new JLabel(new ImageIcon(Config.pathToLoadingGIFAnimation));
		loadingDialog.setSize(200, 200);
		loadingDialog.setLocationRelativeTo(parent);
		loadingDialog.add(gif);
		loadingDialog.setUndecorated(true);
		loadingDialog.isAlwaysOnTop();
		loadingDialog.setVisible(true);	
	}
	
	class ComputationThread extends SwingWorker<Boolean, Void>{
		private OptimMethod optimAlgorithm;
		private ParamStruct params;
		
		public ComputationThread(OptimMethod optimAlgorithm, ParamStruct params){
			this.optimAlgorithm = optimAlgorithm;
			this.params = params;
		}
		
		@Override
		protected Boolean doInBackground() throws Exception {
			mainController.dispatchOptimizeCall(optimAlgorithm,params);
			return true;
		}
		
		@Override
		protected void done() {
			try {
				this.get();
				loadingDialog.setVisible(false);
				loadingDialog.dispose();
				loadingDialog = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	class OptimizeButonActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			loadingDialog = new JDialog();
			initializeDialog(loadingDialog, MainWindow.this);
			
			OptimMethod optimAlgorithm = OptimMethod.NOT_DEFINED;
			ParamStruct params = null;
			
			if (evt.getSource() == psoButton){
				optimAlgorithm = OptimMethod.PSO;
				params = new PSOparamStruct();
				
				((PSOparamStruct)params).setMaxIterations(Integer.parseInt(PSOmaxIterations.getText()));
				((PSOparamStruct)params).setSwarmSize(Integer.parseInt(PSOswarmSize.getText()));
			}
			else if(evt.getSource() == gwoButton){
				optimAlgorithm = OptimMethod.GWO;
				params = new GWOparamStruct();
				
				((GWOparamStruct)params).setMaxIterations(Integer.parseInt(GWOmaxIterations.getText()));
				((GWOparamStruct)params).setWolfPackSize(Integer.parseInt(GWOpackSize.getText()));
			}
			else if(evt.getSource() == deButton){
				optimAlgorithm = OptimMethod.DE;
				params = new DEparamStruct();
				
				((DEparamStruct)params).setMaxIterations(Integer.parseInt(DEmaxIterations.getText()));
				((DEparamStruct)params).setPopSize(Integer.parseInt(DEpopSize.getText()));
				
				int strategy = 0;
				if(DEstrat1.isSelected()){
					strategy = 1;
				}
				else if(DEstrat3.isSelected()){
					strategy = 3;
				}
				else{
					strategy = 2;
				}
				((DEparamStruct)params).setStrategy(strategy);
				
				((DEparamStruct)params).setCoAdaptationSpeed(Double.parseDouble(DEcrossOverAdaptation.getText()));
				((DEparamStruct)params).setCrossOverProb(Double.parseDouble(DEcrossOverProb.getText()));
				((DEparamStruct)params).setDifferetialWightFactor(Double.parseDouble(DEdifferentialWeight.getText()));
			}
			else if(evt.getSource() == gaButton){
				optimAlgorithm = OptimMethod.GA;
				params = new GAparamStruct();
				
				((GAparamStruct)params).setPopSize(Integer.parseInt(GApopSize.getText()));
				((GAparamStruct)params).setMaxIterations(Integer.parseInt(GAmaxIterations.getText()));
				((GAparamStruct)params).setpCrossOver(Double.parseDouble(GAcrossOverProb.getText()));
				((GAparamStruct)params).setpMutation(Double.parseDouble(GAmutProb.getText()));
				((GAparamStruct)params).setElitism(Double.parseDouble(GAelitism.getText()));
			}
			else{
				System.out.println("ERR: Unknown optim method chosen!");
			}
			
			Bounds bounds = new Bounds();
			bounds.setEpsilonLowerBound(Double.parseDouble(minEps.getText()));
			bounds.setEpsilonUpperBound(Double.parseDouble(maxEps.getText()));
			bounds.setCostLowerBound(Double.parseDouble(minCost.getText()));
			bounds.setCostUpperBound(Double.parseDouble(maxCost.getText()));
			bounds.setGammaLowerBound(Double.parseDouble(minGamma.getText()));
			bounds.setGammaUpperBound(Double.parseDouble(maxGamma.getText()));
			
			if(!bounds.checkBounds()){
				JOptionPane.showMessageDialog(MainWindow.this,"Upper bounds should be larger that lower bounds.","Bounds error", JOptionPane.ERROR_MESSAGE);
				return;
			}
			params.setBounds(bounds);
			params.setNoOfFolds(Integer.parseInt(validationFoldsNo.getText()));
			
			SwingWorker<Boolean, Void> worker = new ComputationThread(optimAlgorithm, params);
			worker.execute();
		}
	}
	
}

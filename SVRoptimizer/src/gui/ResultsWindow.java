package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Config;
import optimizer.utils.OptimResult;
import optimizer.utils.PredictionResult;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import utils.Util;

@SuppressWarnings("serial")
public class ResultsWindow extends JFrame {
	private ChartPanel predictionChartPanel;
	private ChartPanel convergenceChartPanel;
	private JPanel resultsPanel;
	private JLabel epsLabel;
	private JLabel costLabel;
	private JLabel gammaLabel;
	private JLabel errLabel;
	private JLabel errLabel2;
	
	public ResultsWindow(OptimResult optimResult, PredictionResult predictionResult) {
		super(optimResult.getOptimAlg().name()+" optimization");
		this.setIconImage(new ImageIcon(Config.iconPath).getImage());
		this.setSize(800, 750);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLayout(new FlowLayout());
		
		resultsPanel = new JPanel();
		resultsPanel.setPreferredSize(new Dimension(770,50));
		resultsPanel.setBorder(BorderFactory.createTitledBorder("Results of optimalization"));
		
		epsLabel = new JLabel("Epsilon: "+String.format("%.6f", optimResult.getEpsilon())+"   ");
		costLabel = new JLabel("Cost: "+String.format("%.6f", optimResult.getCost())+"   ");
		gammaLabel = new JLabel("Gamma: "+String.format("%.6f", optimResult.getGamma())+"   ");
		errLabel = new JLabel("Error (RMSE): "+String.format("%.6f", predictionResult.getRMSE())+"   ");
		errLabel2 = new JLabel("Error (MAPE): "+String.format("%.3f", predictionResult.getMAPE()*100)+"%");
		
		resultsPanel.add(epsLabel);
		resultsPanel.add(costLabel);
		resultsPanel.add(gammaLabel);
		resultsPanel.add(errLabel);
		resultsPanel.add(errLabel2);
		
		System.out.println("RMSE check "+optimResult.getError()+" "+predictionResult.getRMSE());
		
		this.add(resultsPanel);
		this.initializeGraphs(optimResult, predictionResult);
	}

	private void initializeGraphs(OptimResult optimResult, PredictionResult predictionResult) {
		XYSeries realData = new XYSeries("Real data");
		XYSeries prediction = new XYSeries(optimResult.getOptimAlg().name() + " prediction");
		XYSeries convergence = new XYSeries(optimResult.getOptimAlg().name() + " convergence");
		
		double[] predictedDataArray = predictionResult.getDataset().get(0).predictedData;
		double[] realDataArray = predictionResult.getDataset().get(0).realData;
		
		if(predictedDataArray.length != realDataArray.length){
			System.out.println("Err: zla predikcia alebo daco predictedLength="+predictedDataArray.length+" realLength "+realDataArray.length);
			return;
		}
		
		for(int i=0; i< realDataArray.length; i++) {
			prediction.add(i,predictedDataArray[i]);
			realData.add(i,realDataArray[i]);
		}
		
		System.out.println(optimResult);
		
		double[] convergenceInfo = optimResult.getConvergenceInfo();
		for(int i=0; i<convergenceInfo.length; i++){
			convergence.add(i,convergenceInfo[i]);
		}
		
		XYSeriesCollection predictionDataSet = new XYSeriesCollection();
		XYSeriesCollection convergenceDataSet = new XYSeriesCollection();
		
		predictionDataSet.addSeries(prediction);
		predictionDataSet.addSeries(realData);
		
		convergenceDataSet.addSeries(convergence);
		
		JFreeChart predictionChart = ChartFactory.createXYLineChart("Prediction of SVR using "+optimResult.getOptimAlg().name(), "Time", "kWh", predictionDataSet,PlotOrientation.VERTICAL, true, true, false);
		JFreeChart convergenceChart = ChartFactory.createXYLineChart("Convergence of "+optimResult.getOptimAlg().name(), "Iteration", "Fitness", convergenceDataSet,PlotOrientation.VERTICAL, true, true, false);
		
		predictionChart.getPlot().setBackgroundAlpha(0.5f);
		convergenceChart.getPlot().setBackgroundAlpha(0.3f);
		
		Plot plot = convergenceChart.getPlot();
		ValueAxis xAxis = ((XYPlot)plot).getDomainAxis(); 
		xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		//To change the lower bound of Y-axis       	
		NumberAxis yAxis = (NumberAxis) ((XYPlot)plot).getRangeAxis();
	    yAxis.setLowerBound(Util.arrayMin(optimResult.getConvergenceInfo())-50);
		
	    Plot plot2 = predictionChart.getPlot();
	    NumberAxis yAxis2 = (NumberAxis) ((XYPlot)plot2).getRangeAxis();
	    double predMin = Util.arrayMin(realDataArray) < Util.arrayMin(predictedDataArray)? Util.arrayMin(realDataArray):Util.arrayMin(predictedDataArray);
	    yAxis2.setLowerBound(predMin-300);
	    
		predictionChartPanel = new ChartPanel(predictionChart);
		convergenceChartPanel = new ChartPanel(convergenceChart);
		
		predictionChartPanel.setPreferredSize(new Dimension(770,320));
		convergenceChartPanel.setPreferredSize(new Dimension(770,320));
		this.add(predictionChartPanel);
		this.add(convergenceChartPanel);
	}
}

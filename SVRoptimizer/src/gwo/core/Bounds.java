package gwo.core;

public class Bounds {
	private double epsilonUpperBound;
	private double epsilonLowerBound;
	
	private double costUpperBound;
	private double costLowerBound;
	
	private double gammaUpperBound;
	private double gammaLowerBound;
	
	public Bounds(){
		this.epsilonLowerBound = 0;
		this.epsilonUpperBound = 0;
		this.costLowerBound = 0;
		this.costUpperBound = 0;
		this.gammaLowerBound = 0;
		this.gammaUpperBound = 0;
	}
	
	public void setUpperBounds(double[] upperBounds){
		this.epsilonUpperBound = upperBounds[0];
		this.costUpperBound = upperBounds[1];
		this.gammaUpperBound = upperBounds[2];
	}
	
	public void setLowerBounds(double[] lowerBounds){
		this.epsilonLowerBound = lowerBounds[0];
		this.costLowerBound = lowerBounds[1];
		this.gammaLowerBound = lowerBounds[2];
	}
	
	public boolean checkBounds(){		
		if( this.epsilonLowerBound > this.epsilonUpperBound 
		 || this.costLowerBound > this.costUpperBound
		 || this.gammaLowerBound > this.gammaUpperBound){
			return false;
		}
		
		return true;
	}
	
	public double getEpsilonUpperBound() {
		return epsilonUpperBound;
	}
	public void setEpsilonUpperBound(double epsilonUpperBound) {
		this.epsilonUpperBound = epsilonUpperBound;
	}
	public double getEpsilonLowerBound() {
		return epsilonLowerBound;
	}
	public void setEpsilonLowerBound(double epsilonLowerBound) {
		this.epsilonLowerBound = epsilonLowerBound;
	}
	public double getCostUpperBound() {
		return costUpperBound;
	}
	public void setCostUpperBound(double costUpperBound) {
		this.costUpperBound = costUpperBound;
	}
	public double getCostLowerBound() {
		return costLowerBound;
	}
	public void setCostLowerBound(double costLowerBound) {
		this.costLowerBound = costLowerBound;
	}
	public double getGammaUpperBound() {
		return gammaUpperBound;
	}
	public void setGammaUpperBound(double gammaUpperBound) {
		this.gammaUpperBound = gammaUpperBound;
	}
	public double getGammaLowerBound() {
		return gammaLowerBound;
	}
	public void setGammaLowerBound(double gammaLowerBound) {
		this.gammaLowerBound = gammaLowerBound;
	}
}

package gwo.core;

import gwo.evalfunctions.Evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GreyWolfOptimizer {
	private Wolf alpha;
	private Wolf beta;
	private Wolf delta;
	private List<Wolf> wolfPack;
	private List<Wolf> convergenceInfo = null;
	
	public GreyWolfOptimizer(){}
	
	private void initializeWolfPack(int wolvesNo, Bounds bounds){
		// Initialize leaders
		alpha = new Wolf();
		beta  = new Wolf();
		delta = new Wolf();
		
		this.wolfPack = new ArrayList<Wolf>();
		this.convergenceInfo = new ArrayList<Wolf>();
		
		// Initialize random positions of wolves from wolfpack
		double epsPos, costPos, gammaPos;
		Random rand = new Random(System.currentTimeMillis());
		for(int i=0; i<wolvesNo; i++){
			epsPos   = rand.nextDouble()*(bounds.getEpsilonUpperBound() - bounds.getEpsilonLowerBound()) + bounds.getEpsilonLowerBound();
			costPos  = rand.nextDouble()*(bounds.getCostUpperBound() - bounds.getCostLowerBound()) + bounds.getCostLowerBound();
			gammaPos = rand.nextDouble()*(bounds.getGammaUpperBound() - bounds.getGammaLowerBound()) + bounds.getGammaLowerBound();
		
			this.wolfPack.add(new Wolf(epsPos, costPos, gammaPos));
		}
	}
	
	private void returnWolfIntoBounds(Wolf wolf, Bounds bounds){
		// Epsilon
		if(wolf.getPosition()[0] > bounds.getEpsilonUpperBound()){
			wolf.setEpsilon(bounds.getEpsilonUpperBound());
		}
		else if(wolf.getPosition()[0] < bounds.getEpsilonLowerBound()){
			wolf.setEpsilon(bounds.getEpsilonLowerBound());
		}
		
		// Cost
		if(wolf.getPosition()[1] > bounds.getCostUpperBound()){
			wolf.setCost(bounds.getCostUpperBound());
		}
		else if(wolf.getPosition()[1] < bounds.getCostLowerBound()){
			wolf.setCost(bounds.getCostLowerBound());		
		}
		
		// Gamma
		if(wolf.getPosition()[2] > bounds.getGammaUpperBound()){
			wolf.setGamma(bounds.getGammaUpperBound());
		}
		else if(wolf.getPosition()[2] < bounds.getGammaLowerBound()){
			wolf.setGamma(bounds.getGammaLowerBound());
		}
	}
	
	public Wolf optimize(int maxIterations, int wolvesNo, Bounds bounds, Evaluation evalFunction) throws Exception{
		double a;
		if(!bounds.checkBounds()) throw new Exception("ERR: Wrong bounds. Wrongly defined search space");
		
		this.initializeWolfPack(wolvesNo, bounds);
		for(int m=0; m<maxIterations; m++){

			for(Wolf current : this.wolfPack){			
				
				this.returnWolfIntoBounds(current, bounds);
				
				double fitness = evalFunction.evaluateWolf(current);
				current.setFitness(fitness);

				if(fitness < alpha.getFitness()){
					alpha = current.clone();
					//System.out.println("Alpha was set> "+current);
				}
				else if(fitness < beta.getFitness()){
					beta = current.clone();
					//System.out.println("Beta was set> "+current);
				}
				else if(fitness < delta.getFitness()){
					delta = current.clone();
					//System.out.println("Delta was set> "+current);
				}
			}
			
			a = 2 - m * (2 / maxIterations);
			for (Wolf current : this.wolfPack) {
				double[] positions = current.getPosition();
				
				double A1,A2,A3;
				double C1,C2,C3;
				double D_alpha,D_beta,D_delta;
				double X1,X2,X3;
				double newPosition;
				Random rand = new Random(System.currentTimeMillis());
				
				for (int p = 0; p < positions.length; p++) {
					
					// Computation of equations for current and alpha
					A1 = this.computeParamA(rand, a);
					C1 = this.computeParamC(rand);
					D_alpha = this.computeParamD(alpha, current, p, C1);
					X1 = this.computeParamX(alpha, p, A1, D_alpha);
					
					// Computation of equations for current and beta
					A2 = this.computeParamA(rand, a);
					C2 = this.computeParamC(rand);
					D_beta = this.computeParamD(beta, current, p, C2);
					X2 = this.computeParamX(beta, p, A2, D_beta);
					
					// Computation of equations for current and delta
					A3 = this.computeParamA(rand, a);
					C3 = this.computeParamC(rand);
					D_delta = this.computeParamD(delta, current, p, C3);
					X3 = this.computeParamX(delta, p, A3, D_delta);

					newPosition = (X1 + X2 + X3) / 3;
					current.setPositionOnIndex(p, newPosition);
				}
			}
			
			System.out.println("Iteration: "+ m +" fit: "+alpha.getFitness()+" best: "+ alpha);
			convergenceInfo.add(alpha.clone());
		}
		
		return alpha;
	}
	
	private double computeParamX(Wolf leader, int positionIndex, double paramA, double paramD){
		 return leader.getPosition()[positionIndex] - (paramA * paramD);
	}
	
	private double computeParamD(Wolf leader, Wolf current, int paramIndex, double paramC){
		return Math.abs((paramC * leader.getPosition()[paramIndex]) - current.getPosition()[paramIndex]);
	}
	
	private double computeParamA(Random rand, double a){
		return  (2 * a * rand.nextDouble()) - a;
	}
	
	private double computeParamC(Random rand){
		return  (rand.nextDouble() * 2);
	}

	public List<Wolf> getConvergenceInfo() throws Exception {
		if(convergenceInfo == null){
			throw new Exception("Err: No convergenceInfo found");
		}
		return convergenceInfo;
	}

}

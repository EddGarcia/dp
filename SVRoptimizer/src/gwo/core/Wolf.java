package gwo.core;

public class Wolf {
	private double fitness;
	private double[] position;
	
	public Wolf(){
		fitness = Double.MAX_VALUE;
		position = new double[]{0.0, 0.0, 0.0};
	}
	
	public Wolf(double[] position){
		fitness = Double.MAX_VALUE;
		this.position = position;
	}
	
	public Wolf(double epsPos, double costPos, double gammaPos){
		fitness = Double.MAX_VALUE;
		this.position = new double[3];
		this.position[0] = epsPos;
		this.position[1] = costPos;
		this.position[2] = gammaPos;
	}

	public boolean setPositionOnIndex(int index, double position){
		if(index >= this.position.length) return false;
		
		this.position[index] = position;
		return true;
	}
	
	public double getEpsilon(){
		return position[0];
	}
	
	public double getCost(){
		return position[1];
	}
	
	public double getGamma(){
		return position[2];
	}
	
	public void setEpsilon(double epsilon){
		this.position[0] = epsilon;
	}
	
	public void setCost(double cost){
		this.position[1] = cost;
	}
	
	public void setGamma(double gamma){
		this.position[2] = gamma;
	}
	
	public double getFitness() {
		return fitness;
	}

	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	public double[] getPosition() {
		return position;
	}

	public void setPosition(double[] position) {
		this.position = position;
	}
	
	public String toString(){
		return new String("Params: eps="+this.position[0]+" cost="+this.position[1]+" gamma="+this.position[2]);
	}
	
	public Wolf clone(){
		Wolf clone = new Wolf();
		clone.setFitness(this.fitness);
		clone.setPosition(this.position.clone());
		return clone;
	}
}

package gwo.evalfunctions;

import gwo.core.Wolf;

public interface Evaluation {	
	public double evaluateWolf(Wolf wolf) throws Exception;
}

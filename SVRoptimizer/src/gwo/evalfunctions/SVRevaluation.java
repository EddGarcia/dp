package gwo.evalfunctions;

import gwo.core.Wolf;

import java.util.ArrayList;
import java.util.List;

import utils.Metrics;
import utils.Predictor;
import utils.RConnector;

public class SVRevaluation implements Evaluation{
	private List<double[]> dataSet;
	
	public SVRevaluation(int numOfFolds){
		dataSet = new ArrayList<double[]>();
		
		try{
			for(int i=0; i<numOfFolds; i++){
				dataSet.add(RConnector.getRengine().eval("c(getDay(7*"+i+"+0),getDay(7*"+i+"+1),getDay(7*"+i+"+2))").asDoubleArray());
				dataSet.add(RConnector.getRengine().eval("getDay(7*"+i+"+3)").asDoubleArray());
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public double evaluateWolf(Wolf wolf) throws Exception{
	    double cumulativeError = 0;
		
		for(int i=0; i<dataSet.size(); i+=2){
	    	cumulativeError += evaluateParameters(dataSet.get(i+1),dataSet.get(i), wolf.getEpsilon(), wolf.getGamma(), wolf.getCost());
	    }
		return cumulativeError/(dataSet.size()/2);
	}
	
	public double evaluateParameters(double[] realData, double[] trainData, double epsilon, double gamma, double cost) throws Exception {
		Predictor SVRpredictor = new Predictor();
		double[] prediction = SVRpredictor.predict(trainData, epsilon, cost, gamma);
		return Metrics.RMSEcalculator(prediction, realData);
	}
}

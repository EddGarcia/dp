package loader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.jfree.data.xy.XYSeries;

import utils.LabeledTimeSeries;


public class CSVTimeSeriesDataLoader extends DataLoader{
	
	public LabeledTimeSeries LoadData(String pathToSourceFile, String seriesLabel) throws Exception{
		if(!this.FileExists(pathToSourceFile)){
			throw new FileNotFoundException("ERR: Subor sa nenasiel.");
		}
		
		BufferedReader reader = null;
		String line = "";
		String separator = ",";
		XYSeries timeseries = new XYSeries(seriesLabel);
		
		try {
			reader = new BufferedReader(new FileReader(pathToSourceFile));
			String[] header = reader.readLine().split(separator);
			String[] tokens;
			int i = 0;
			
			while((line = reader.readLine()) != null) {
				tokens = line.split(separator);
				// TODO data specific replacement of -
				if(tokens[0].isEmpty() || tokens[1].isEmpty()){
					break;
				}
				timeseries.add(i++/*Integer.parseInt(tokens[0].replaceAll("-", "").replaceAll("\"",""))*/, Integer.parseInt(tokens[1]));
			}
			
			return new LabeledTimeSeries(seriesLabel,header[0],header[1],timeseries);
		} 
		catch(IOException e) {
			throw new IOException("ERR: Nastala chyba pri citani suboru.");
		}
		catch(Exception e){
			throw new Exception("ERR: Nastala ina chyba pri nacitavani casoveho radu.");
		}
		finally {
			reader.close();
		}
	}
}

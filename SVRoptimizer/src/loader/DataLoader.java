package loader;

import java.io.File;

import utils.LabeledTimeSeries;

public abstract class DataLoader {
	
	public boolean FileExists(String path){
		File file = new File(path);
		if(file.exists() && !file.isDirectory()) { 
		    return true;
		}
		return false;
	}
	
	abstract public LabeledTimeSeries LoadData(String pathToSourceFile, String seriesLabel) throws Exception;
}

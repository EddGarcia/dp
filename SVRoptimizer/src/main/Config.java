package main;

public class Config {
	public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/95_partizanske_suma.csv";
	
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/01_zilina_suma.csv";
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/02_cadca_suma.csv";
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/03_martin_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/04_kosice_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/05_poprad_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/06_humenne_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/07_trebisov_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/08_presov_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/8_ba_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/8_ba5psc_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/90_zahorie_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/91_trnava_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/92_piestany_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/93_dun-streda_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/94_nitra_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/95_partizanske_suma.csv";
	//+public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/96_zvolen_suma.csv";
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/97_bb_suma.csv";
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/98_rim-sobota_suma.csv";
	//public final static String pathToCSVData = "C:/Users/Eduard/Desktop/data podla PSC/99_velky-krtis_suma.csv";
	

	public final static String pathToDataLoader = "C:/dp/SVRoptimizer/dataSourceLoader/dataLoader.R";
	
	public final static String pathToSVRPredictionScript = "C:/dp/SVRoptimizer/metaheuristics/svrPrediction.R";
	public final static String pathToSinDummyDataScript = "C:/dp/SVRoptimizer/dataSourceLoader/sinDataGenerator.R";
	
	public final static String pathToPSOoptimizer = "C:/dp/SVRoptimizer/metaheuristics/PSO.R";
	public final static String pathToDEoptimizer = "C:/dp/SVRoptimizer/metaheuristics/DE.R";
	public final static String pathToGAoptimizer = "C:/dp/SVRoptimizer/metaheuristics/GA.R";
	
	public final static String pathToLoadingGIFAnimation = "C:/dp/SVRoptimizer/img/loader.gif";
	public final static String iconPath = "C:/dp/SVRoptimizer/img/icon.png";
	
	// Bounds
	public final static double epsilonMinValue = 0.00001;
	public final static double epsilonMaxValue = 1;
	
	public final static double costMinValue = 0.001;
	public final static double costMaxValue = 10;
	
	public final static double gammaMinValue = 0.001;
	public final static double gammaMaxValue = 2;
	
	public final static int noOfFolds = 5;
	
	//PSO
	public final static int PSOswarmSize = 20;
	public final static int PSOmaxIterations = 30;
	
	//GWO
	public final static int GWOpackSize = 20;
	public final static int GWOmaxIterations = 30;
	
	//DE
	public final static int DEpopSize = 20;
	public final static int DEmaxIterations = 30;
	public final static double DEcrossOverProb = 0.5;
	public final static double DEdifferetialWightFactor = 0.8;
	public final static double DEcoAdaptationSpeed = 0.5;
	
	//GA
	public final static int GApopSize = 30;
	public final static int GAmaxIterations = 30;
	public final static double GAElitism = 4;
	public final static double GACrossOver = 0.8;
	public final static double GAMutation = 0.2;
}

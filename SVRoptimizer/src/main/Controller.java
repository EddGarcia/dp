package main;

import gui.MainWindow;
import gui.ResultsWindow;

import java.util.ArrayList;
import java.util.List;

import optimizer.methods.DE;
import optimizer.methods.GA;
import optimizer.methods.GWO;
import optimizer.methods.Optimizer;
import optimizer.methods.PSO;
import optimizer.paramstructs.ParamStruct;
import optimizer.utils.Dataset;
import optimizer.utils.OptimMethod;
import optimizer.utils.OptimResult;
import optimizer.utils.PredictionResult;
import optimizer.utils.Result;
import utils.Predictor;
import utils.RConnector;

public class Controller {
	private MainWindow app;
	
	public void initMainWindow() {
		this.app = new MainWindow(this);
		app.setLocationRelativeTo(null);
		app.setVisible(true);
	}
	
	public void loadRequiredRScripts(){
		try{
			RConnector.getRengine().eval("source('"+Config.pathToSVRPredictionScript+"')");		
			RConnector.getRengine().eval("source('"+Config.pathToDataLoader+"')");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadCsvData(String pathToCsv){
		try{
			RConnector.getRengine().eval("loadData <- read.csv('"+pathToCsv+"',header = TRUE, sep = \",\" )");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Result dispatchOptimizeCall(OptimMethod optAlgorithm, ParamStruct params) {
		ResultsWindow resultsDisplay = null;
		Optimizer optimizer = null;
		OptimResult optimResult = null;
		PredictionResult predictionResult = null;
		Predictor SVRpredictor = new Predictor();
		//double[] predictedData = null;
		//double[] realData = null;
		//double[] trainData = null;
		List<Dataset> dataset = new ArrayList<Dataset>();
		
		//Data loading
		try{
			Dataset predDay1 = new Dataset();
			predDay1.realData = RConnector.getRengine().eval("getDay(7*"+params.getNoOfFolds()+"+3)").asDoubleArray(); 
			predDay1.trainData = RConnector.getRengine().eval("c(getDay(7*"+params.getNoOfFolds()+"),getDay(7*"+params.getNoOfFolds()+"+1),getDay(7*"+params.getNoOfFolds()+"+2))").asDoubleArray();
			
			Dataset predDay2 = new Dataset();
			predDay2.realData = RConnector.getRengine().eval("getDay(7*"+(params.getNoOfFolds()+1)+"+3)").asDoubleArray(); 
			predDay2.trainData = RConnector.getRengine().eval("c(getDay(7*"+(params.getNoOfFolds()+1)+"),getDay(7*"+(params.getNoOfFolds()+1)+"+1),getDay(7*"+(params.getNoOfFolds()+1)+"+2))").asDoubleArray();
			
			Dataset predDay3 = new Dataset();
			predDay3.realData = RConnector.getRengine().eval("getDay(7*"+(params.getNoOfFolds()+2)+"+3)").asDoubleArray(); 
			predDay3.trainData = RConnector.getRengine().eval("c(getDay(7*"+(params.getNoOfFolds()+2)+"),getDay(7*"+(params.getNoOfFolds()+2)+"+1),getDay(7*"+(params.getNoOfFolds()+2)+"+2))").asDoubleArray();
		
			dataset.add(predDay1);
			dataset.add(predDay2);
			dataset.add(predDay3);
			
			RConnector.getRengine().eval("dataSet <- lapply(1:"+params.getNoOfFolds()+", function(i) list("+
				  	  "train_data = c(getDay(0+(7*(i-1))),getDay(1+(7*(i-1))),getDay(2+(7*(i-1)))),"+   
				      "test_data  = getDay(3+(7*(i-1)))))");
		}
		catch(Exception e){
			e.printStackTrace();
		}
		//--------------------------
		
		long startTime = System.currentTimeMillis();
		if (optAlgorithm == OptimMethod.PSO) {
			System.out.println("Optim alg PSO");
			
			try {
				RConnector.getRengine().eval("source('"+Config.pathToPSOoptimizer+"')");
				optimizer = new PSO();
				optimizer.initParams(params);
				optimResult = optimizer.optimizeParameters();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		else if (optAlgorithm == OptimMethod.GWO) {
			System.out.println("Optim alg GWO");
			
			try {
				optimizer = new GWO();
				optimizer.initParams(params);
				optimResult = optimizer.optimizeParameters();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		else if (optAlgorithm == OptimMethod.DE) {
			System.out.println("Optim alg DE");
			
			try {
				RConnector.getRengine().eval("source('"+Config.pathToDEoptimizer+"')");
				optimizer = new DE();
				optimizer.initParams(params);
				optimResult = optimizer.optimizeParameters();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if (optAlgorithm == OptimMethod.GA) {
			System.out.println("Optim alg GA");
			
			try {
				RConnector.getRengine().eval("source('"+Config.pathToGAoptimizer+"')");
				optimizer = new GA();
				optimizer.initParams(params);
				optimResult = optimizer.optimizeParameters();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		else {
			System.out.println("ERR: Optim alg out of bounds");
		}
		
		try{
			for(Dataset d:dataset){
				d.predictedData = SVRpredictor.predict(d.trainData, optimResult.getEpsilon(), optimResult.getCost(), optimResult.getGamma());
			}
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Err: problem in prediction");
		
		}
		long difTime = System.currentTimeMillis() - startTime;
		System.out.println("Optimization took> "+difTime+" ms ("+difTime/60000.0+"min)");
		
		predictionResult = new PredictionResult();
		predictionResult.setDataset(dataset);

		
		resultsDisplay = new ResultsWindow(optimResult, predictionResult);
		resultsDisplay.setLocationRelativeTo(app);
		resultsDisplay.setVisible(true);
		
		return new Result(optimResult,predictionResult);
	}
}

package main;

public class Runner {
	public static void main(String[] args){
		try{
			Controller c = new Controller();
			c.initMainWindow();
			c.loadRequiredRScripts();
			c.loadCsvData(Config.pathToCSVData);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}

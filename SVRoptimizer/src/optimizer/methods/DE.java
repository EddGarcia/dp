package optimizer.methods;

import java.util.Arrays;

import optimizer.paramstructs.DEparamStruct;
import optimizer.paramstructs.ParamStruct;
import optimizer.utils.OptimMethod;
import optimizer.utils.OptimResult;

import org.rosuda.JRI.Rengine;

import utils.RConnector;

public class DE implements Optimizer{
	private DEparamStruct params;
	
	@Override
	public void initParams(ParamStruct params){
		this.params = (DEparamStruct)params;
	}
	
	@Override
	public OptimResult optimizeParameters() throws Exception {
		Rengine rsession = RConnector.getRengine();
		
		// First three fields are optimized parameters and next fields are convergence info
		double[] results = rsession.eval("tuneSVRUsingDE(dataSet,"+params.getPopSize()+","+params.getMaxIterations()+","+
				params.getStrategy()+","+
				params.getCrossOverProb()+","+
				params.getDifferetialWightFactor()+","+
				params.getCoAdaptationSpeed()+","+
				params.getBounds().getEpsilonLowerBound()+","+
				params.getBounds().getEpsilonUpperBound()+","+
				params.getBounds().getCostLowerBound()+","+
				params.getBounds().getCostUpperBound()+","+
				params.getBounds().getGammaLowerBound()+","+
				params.getBounds().getGammaUpperBound()+")").asDoubleArray();
		
		OptimResult optimizationResult = new OptimResult();
		optimizationResult.setOptimAlg(OptimMethod.DE);
		optimizationResult.setCost(results[0]);
		optimizationResult.setGamma(results[1]);
		optimizationResult.setEpsilon(results[2]);
		optimizationResult.setConvergenceInfo(Arrays.copyOfRange(results, 3, results.length));
		
		System.out.println(optimizationResult);
		
		return optimizationResult;
	}

}

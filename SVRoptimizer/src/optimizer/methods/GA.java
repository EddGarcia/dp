package optimizer.methods;

import java.util.Arrays;

import optimizer.paramstructs.GAparamStruct;
import optimizer.paramstructs.ParamStruct;
import optimizer.utils.OptimMethod;
import optimizer.utils.OptimResult;

import org.rosuda.JRI.Rengine;

import utils.RConnector;

public class GA implements Optimizer{
	private GAparamStruct params;
	
	@Override
	public void initParams(ParamStruct params){
		this.params = (GAparamStruct)params;
	}
	
	@Override
	public OptimResult optimizeParameters() throws Exception {
		Rengine rsession = RConnector.getRengine();
		
		// First three fields are optimized parameters and next fields are convergence info
		double[] results = rsession.eval("tuneSVRUsingGA(dataSet,"+params.getPopSize()+","+params.getMaxIterations()+","+
				params.getElitism()+","+
				params.getpCrossOver()+","+
				params.getpMutation()+","+
				params.getBounds().getEpsilonLowerBound()+","+
				params.getBounds().getEpsilonUpperBound()+","+
				params.getBounds().getCostLowerBound()+","+
				params.getBounds().getCostUpperBound()+","+
				params.getBounds().getGammaLowerBound()+","+
				params.getBounds().getGammaUpperBound()+")").asDoubleArray();
		
		for(int i=3; i<results.length; i++){
			results[i] *= -1.0f;
		}
		
		OptimResult optimizationResult = new OptimResult();
		optimizationResult.setOptimAlg(OptimMethod.GA);
		optimizationResult.setCost(results[0]);
		optimizationResult.setGamma(results[1]);
		optimizationResult.setEpsilon(results[2]);
		optimizationResult.setConvergenceInfo(Arrays.copyOfRange(results, 3, results.length));

		System.out.println(optimizationResult);
		return optimizationResult;
	}
}

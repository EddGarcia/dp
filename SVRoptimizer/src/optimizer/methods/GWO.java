package optimizer.methods;

import gwo.core.GreyWolfOptimizer;
import gwo.core.Wolf;
import gwo.evalfunctions.Evaluation;
import gwo.evalfunctions.SVRevaluation;

import java.util.List;

import optimizer.paramstructs.GWOparamStruct;
import optimizer.paramstructs.ParamStruct;
import optimizer.utils.OptimMethod;
import optimizer.utils.OptimResult;

public class GWO implements Optimizer{
	private GWOparamStruct params;

	@Override
	public void initParams(ParamStruct params){
		this.params = (GWOparamStruct)params;
	}
	
	@Override
	public OptimResult optimizeParameters() throws Exception {
		GreyWolfOptimizer gwo = new GreyWolfOptimizer();
		OptimResult optimResult = new OptimResult();
		Wolf lastAlpha;
		
		Evaluation evalFn = new SVRevaluation(params.getNoOfFolds());
		lastAlpha = gwo.optimize(params.getMaxIterations(), params.getWolfPackSize(), params.getBounds(), evalFn);
		
		List<Wolf> conv = gwo.getConvergenceInfo();
		double[] convergence = new double[conv.size()];
		
		for(int i=0; i< conv.size(); i++){
			convergence[i] = conv.get(i).getFitness();
		}
		
		optimResult.setOptimAlg(OptimMethod.GWO);
		optimResult.setConvergenceInfo(convergence);
		optimResult.setEpsilon(lastAlpha.getEpsilon());
		optimResult.setCost(lastAlpha.getCost());
		optimResult.setGamma(lastAlpha.getGamma());
		
		return optimResult;
	}

}

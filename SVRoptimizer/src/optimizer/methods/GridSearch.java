package optimizer.methods;

import gwo.core.Bounds;

import java.io.PrintWriter;

import main.Config;

import org.rosuda.JRI.Rengine;

import utils.Metrics;
import utils.RConnector;

public class GridSearch{
	private Bounds bounds;
	private double[] stepSize;
	PrintWriter writer;
	
	public void makeDataFileFromGridsearch() throws Exception{
		bounds = new Bounds();
		bounds.setLowerBounds(new double[] {Config.epsilonMinValue,Config.costMinValue,Config.gammaMinValue});
		bounds.setUpperBounds(new double[] {Config.epsilonMaxValue,Config.costMaxValue,Config.gammaMaxValue});
		stepSize = new double[] {0.01,0.1,0.01};
		
		writer = new PrintWriter("gridSearchData.txt","UTF-8");
		writer.println("epsilon, cost, gamma, error");
		
		Rengine rsession = RConnector.getRengine();
		
		rsession.eval("source('"+Config.pathToSinDummyDataScript+"')");
		rsession.eval("source('"+Config.pathToSVRPredictionScript+"')");
		
		int indexOfDay = 5;
		double[] realData = RConnector.getRengine().eval("getDay(7*"+indexOfDay+"+3)").asDoubleArray();
		double[] prediction;
		double error;
		int counter = 0;
		double best = Double.MAX_VALUE;
		double bestEps = 0;
		double bestC = 0;
		double bestGamma = 0;
		
		long start = System.currentTimeMillis();
		for(double epsilon = bounds.getEpsilonLowerBound(); epsilon <= bounds.getEpsilonUpperBound(); epsilon+=stepSize[0]){
			for(double cost = bounds.getCostLowerBound(); cost <= bounds.getCostUpperBound(); cost+=stepSize[1]){
				for(double gamma = bounds.getGammaLowerBound(); gamma <= bounds.getGammaUpperBound(); gamma+=stepSize[2]){
					try{
						prediction = rsession.eval("predictionSVR(c(getDay(7*"+indexOfDay+"),getDay(7*"+indexOfDay+"+1),getDay(7*"+indexOfDay+"+2)),"+cost+","+gamma+","+epsilon+")").asDoubleArray();
						error = Metrics.RMSEcalculator(realData, prediction);
					}
					catch(NullPointerException e){
						error = 35000;
					}
					counter++;	
					if(error < best){
						best = error;
						bestC = cost;
						bestEps = epsilon;
						bestGamma = gamma;
					}
					
				    writer.println(String.format("%.5f", epsilon)+","+String.format("%.5f", cost)+","+String.format("%.5f", gamma)+","+String.format("%.6f", error));
				    System.out.println(String.format("%.5f", epsilon)+" "+String.format("%.5f", cost)+" "+String.format("%.5f", gamma)+" "+String.format("%.6f", error)+ " counter> "+counter);
				}
			}
			writer.flush();
		}
		writer.close();
		System.out.println("Running time = "+(start-System.currentTimeMillis()));
		System.out.println(String.format("%.5f", bestEps)+","+String.format("%.5f", bestC)+","+String.format("%.5f", bestGamma)+","+String.format("%.6f", best));
	}
}

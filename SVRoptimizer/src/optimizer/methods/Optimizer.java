package optimizer.methods;

import optimizer.paramstructs.ParamStruct;
import optimizer.utils.OptimResult;

public interface Optimizer {
	public void initParams(ParamStruct params);
	
	public OptimResult optimizeParameters() throws Exception;
}

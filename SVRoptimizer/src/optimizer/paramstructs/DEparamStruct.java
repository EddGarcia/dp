package optimizer.paramstructs;

public class DEparamStruct extends ParamStruct{
	private int popSize;
	private int maxIterations;
	private double crossOverProb;
	private double differetialWightFactor;
	private double coAdaptationSpeed;
	private int strategy;
	
	public int getPopSize() {
		return popSize;
	}
	
	public void setPopSize(int popSize) {
		this.popSize = popSize;
	}
	
	public int getMaxIterations() {
		return maxIterations;
	}
	
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}
	
	public double getCrossOverProb() {
		return crossOverProb;
	}
	
	public void setCrossOverProb(double crossOverProb) {
		if(crossOverProb < 0){
			this.crossOverProb = 0;
		}
		else if(crossOverProb > 1){
			this.crossOverProb = 1;
		}
		else{
			this.crossOverProb = crossOverProb;
		}
	}
	
	public double getDifferetialWightFactor() {
		return differetialWightFactor;
	}
	
	public void setDifferetialWightFactor(double differetialWightFactor) {
		if(differetialWightFactor < 0){
			this.differetialWightFactor = 0;
		}
		else if(differetialWightFactor > 2){
			this.differetialWightFactor = 2;
		}
		else{
			this.differetialWightFactor = differetialWightFactor;
		}
	}
	
	public double getCoAdaptationSpeed() {
		return coAdaptationSpeed;
	}
	
	public void setCoAdaptationSpeed(double coAdaptationSpeed) {
		if(coAdaptationSpeed < 0){
			this.coAdaptationSpeed = 0;
		}
		else if(coAdaptationSpeed > 1){
			this.coAdaptationSpeed = 1;
		}
		else{
			this.coAdaptationSpeed = coAdaptationSpeed;
		}
	}
	
	public int getStrategy() {
		return strategy;
	}
	
	public void setStrategy(int strategyIndex) {
		this.strategy = strategyIndex;
	}
}

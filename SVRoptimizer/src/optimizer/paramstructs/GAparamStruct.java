package optimizer.paramstructs;

public class GAparamStruct extends ParamStruct{
	private int popSize;
	private int maxIterations;
	private double elitism;
	private double pCrossOver;
	private double pMutation;
	
	public int getPopSize() {
		return popSize;
	}
	
	public void setPopSize(int popSize) {
		this.popSize = popSize;
	}
	
	public int getMaxIterations() {
		return maxIterations;
	}
	
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}
	
	public double getElitism() {
		return elitism;
	}
	
	public void setElitism(double elitism) {
		this.elitism = elitism;
	}
	
	public double getpCrossOver() {
		return pCrossOver;
	}
	
	public void setpCrossOver(double pCrossOver) {
		if(pCrossOver < 0){
			this.pCrossOver = 0;
		}
		else if(pCrossOver > 1){
			this.pCrossOver = 1;
		}
		else{
			this.pCrossOver = pCrossOver;
		}
	}
	
	public double getpMutation() {
		return pMutation;
	}
	
	public void setpMutation(double pMutation) {
		if(pMutation < 0){
			this.pMutation = 0;
		}
		else if(pMutation > 1){
			this.pMutation = 1;
		}
		else{
			this.pMutation = pMutation;
		}
	}
}

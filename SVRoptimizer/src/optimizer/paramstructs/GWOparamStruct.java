package optimizer.paramstructs;

public class GWOparamStruct extends ParamStruct{
	private int wolfPackSize;
	private int maxIterations;
	
	public int getWolfPackSize() {
		return wolfPackSize;
	}
	
	public void setWolfPackSize(int wolfPackSize) {
		this.wolfPackSize = wolfPackSize;
	}
	
	public int getMaxIterations() {
		return maxIterations;
	}
	
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}
}

package optimizer.paramstructs;


public class PSOparamStruct extends ParamStruct{
	private int swarmSize;
	private int maxIterations;
	
	public int getSwarmSize() {
		return swarmSize;
	}
	public void setSwarmSize(int swarmSize) {
		this.swarmSize = swarmSize;
	}
	public int getMaxIterations() {
		return maxIterations;
	}
	public void setMaxIterations(int maxIterations) {
		this.maxIterations = maxIterations;
	}
}

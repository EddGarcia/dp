package optimizer.paramstructs;

import gwo.core.Bounds;

public class ParamStruct {
	private Bounds bounds;
	private int noOfFolds;
	
	public int getNoOfFolds() {
		return noOfFolds;
	}

	public void setNoOfFolds(int noOfFolds) {
		this.noOfFolds = noOfFolds;
	}

	public Bounds getBounds() {
		return bounds;
	}

	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}
}

package optimizer.utils;

public class Dataset {
	public double[] realData;
	public double[] trainData;
	public double[] predictedData;
}

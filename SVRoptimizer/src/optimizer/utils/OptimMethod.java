package optimizer.utils;

public enum OptimMethod {
	NOT_DEFINED,
	PSO,
	GA,
	DE,
	GWO,
	GRID_SEARCH;
}

package optimizer.utils;

public class OptimResult {
	private OptimMethod optimAlg;
	private double epsilon;
	private double cost;
	private double gamma;
	private double[] convergenceInfo;

	
	public OptimMethod getOptimAlg() {
		return optimAlg;
	}

	public void setOptimAlg(OptimMethod optimAlg) {
		this.optimAlg = optimAlg;
	}

	public double getEpsilon() {
		return epsilon;
	}

	public void setEpsilon(double epsilon) {
		this.epsilon = epsilon;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getGamma() {
		return gamma;
	}

	public void setGamma(double gamma) {
		this.gamma = gamma;
	}

	public double[] getConvergenceInfo() {
		return convergenceInfo;
	}

	public void setConvergenceInfo(double[] convergenceInfo) {
		this.convergenceInfo = convergenceInfo;
	}
	
	public double getError(){
		if(this.convergenceInfo == null) return -1;
		return this.convergenceInfo[this.convergenceInfo.length-1];
	}
	
	@Override
	public String toString(){
		StringBuilder build = new StringBuilder("\n"+optimAlg.name()+"\n");
		build.append("Optimized params\n---------------\neps:\t"+this.epsilon+"\ncost:\t"+this.cost+"\ngamma:\t"+this.gamma+"\n\nConvergence info\n---------------\n");
		
		for(int i=0; i<this.convergenceInfo.length; i++){
			build.append(i+"\t"+this.convergenceInfo[i]+"\n");
		}
		
		return build.toString();
	}
}

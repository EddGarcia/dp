package optimizer.utils;

import java.util.List;

import utils.Metrics;

public class PredictionResult {
	private List<Dataset> dataset;
	
	
	/*private double[] realData;
	private double[] predictedData;
	
	public double[] getRealData() {
		return realData;
	}
	public void setRealData(double[] realData) {
		this.realData = realData;
	}
	public double[] getPredictedData() {
		return predictedData;
	}
	public void setPredictedData(double[] predictedData) {
		this.predictedData = predictedData;
	}*/
	
	public List<Dataset> getDataset() {
		return dataset;
	}

	public void setDataset(List<Dataset> dataset) {
		this.dataset = dataset;
	}

	public double getMAPE(){
		double sum=0;
		
		for(Dataset d : dataset){
			sum += Metrics.MAPEcalculator(d.realData, d.predictedData);
		}
		
		return (double)(sum/(double)dataset.size());
	}
	
	public double getRMSE(){
		double sum=0;
		
		for(Dataset d : dataset){
			sum += Metrics.RMSEcalculator(d.realData, d.predictedData);
		}
		
		return (double)(sum/(double)dataset.size());
	}
}

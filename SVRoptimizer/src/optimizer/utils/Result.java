package optimizer.utils;

import utils.Metrics;

public class Result {
	private OptimResult optimResult;
	private PredictionResult predictionResult;
	
	public Result(OptimResult optimResult, PredictionResult predictionResult){
		this.optimResult = optimResult;
		this.predictionResult = predictionResult;
	}
	
	public OptimResult getOptimResult() {
		return optimResult;
	}
	public void setOptimResult(OptimResult optimResult) {
		this.optimResult = optimResult;
	}
	public PredictionResult getPredictionResult() {
		return predictionResult;
	}
	public void setPredictionResult(PredictionResult predictionResult) {
		this.predictionResult = predictionResult;
	}
	
	@Override
	public String toString(){
		StringBuilder build = new StringBuilder();
		build.append(optimResult.getEpsilon()+" ");
		build.append(optimResult.getCost()+" ");
		build.append(optimResult.getGamma()+"    ");
		
		for(double d : optimResult.getConvergenceInfo()){
			build.append(d+" ");
		}
		build.append("    ");
		for(Dataset d : predictionResult.getDataset()){
			build.append(Metrics.MAPEcalculator(d.realData, d.predictedData)+" "+Metrics.RMSEcalculator(d.realData, d.predictedData)+" ");
		}
		build.append("    "+predictionResult.getRMSE()+" ");
		build.append(predictionResult.getMAPE());
		return build.toString();
	}
}

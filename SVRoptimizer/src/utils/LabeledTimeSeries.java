package utils;

import org.jfree.data.xy.XYSeries;

public class LabeledTimeSeries {
	private String labelX;
	private String labelY;
	private String title;
	private XYSeries timeseries;
	
	public LabeledTimeSeries(String title, String labelX, String labelY, XYSeries timeseries){
		this.title = title;
		this.labelX = labelX;
		this.labelY = labelY;
		this.timeseries = timeseries;
	}
	
	public LabeledTimeSeries(){
		this.labelX = null;
		this.labelY = null;
		this.timeseries = null;
	}
	
	public String getLabelX() {
		return labelX;
	}
	public void setLabelX(String labelX) {
		this.labelX = labelX;
	}
	public String getLabelY() {
		return labelY;
	}
	public void setLabelY(String labelY) {
		this.labelY = labelY;
	}
	public XYSeries getTimeseries() {
		return timeseries;
	}
	public void setTimeseries(XYSeries timeseries) {
		this.timeseries = timeseries;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String toString(){
		StringBuilder builder = new StringBuilder("Time Series:\n  LabelX - "+this.labelX+"\n  LabelY - "+this.labelY+"\n---Vzorka dat----\n");
		
		for(int i=0; i<10; i++){
			try{
				builder.append(timeseries.getX(i)+","+timeseries.getY(i)+"\n");
			}
			catch(Exception e){
				System.out.println("Err: Chyba pri vypisovani radu.");
			}
		}
		builder.append("...\n");
		return builder.toString();
	}
}

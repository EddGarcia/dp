package utils;

public class Metrics {
	// x - predicted values
	// y - actual values
	
	public static double RMSEcalculator(double[] x, double[] y){
		if(x.length != y.length) return -1;
		
		double sum = 0;
		for(int i=0; i<x.length; i++){
			sum += Math.pow(x[i] - y[i], 2);
		}
		
		double mean = sum/(double)x.length;
		return Math.sqrt(mean);
	}
	
	public static double MAPEcalculator(double[] x, double[] y){
		if(x.length != y.length) return -1;
		
		double sum = 0;
		for(int i=0; i<x.length; i++){
			sum += (Math.abs(x[i] - y[i])/x[i]);
		}
		
		double mean = sum/(double)x.length;
		return mean;
	}
}

package utils;

import main.Config;

public class Predictor {
	
	public double[] predict(double[] trainData, double epsilon, double cost, double gamma) throws Exception {
		RConnector.getRengine().eval("source('"+Config.pathToSVRPredictionScript+"')");
		
		RConnector.getRengine().eval("x<-vector(mode=\"numeric\",length="+trainData.length+")");
		for(int i=1; i<=trainData.length; i++){
			RConnector.getRengine().eval("x["+i+"] <- as.numeric("+trainData[i-1]+")");
		}
		
		try{
			double[] prediction = RConnector.getRengine().eval("predictionSVR(as.vector(x),"+cost+","+gamma+","+epsilon+")").asDoubleArray();
			return prediction;
		}
		catch(NullPointerException e){
			e.printStackTrace();
			System.out.println("ERR: No model could have been created by using \neps: "+epsilon+"\ncost: "+cost+"\ngamma: "+gamma);
		}
		
		return null;
	}
}

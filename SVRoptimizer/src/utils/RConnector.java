package utils;

import org.rosuda.JRI.Rengine;

public final class RConnector {
	private static Rengine rsession = null;
	
	public static Rengine getRengine() throws Exception{
		if(rsession == null) {
			initializeRSession();
		}
		return rsession;
	}
	
	//TODO delete in production version
	public static double[] getData(){
		return rsession.eval("makeSinDummyData(3,96)").asDoubleArray();
	}
	
	private static void initializeRSession() throws Exception {
		Rengine rengine = new Rengine (new String [] {"--vanilla"}, false, null);
	    
	    if (!rengine.waitForR()) {
	      System.out.println ("ERROR> cannot load R ");
	      throw new Exception("Cannot initialize RSession.");
	    }
	    
	    rsession = rengine;
	}
	
	public static boolean RSessionExists(){
		if(rsession != null){
			return true;
		}
		else{
			return false;
		}
	}
	
	public static void closeRSession() throws Exception{
		if(rsession == null){
			throw new Exception("No session to close.");
		}
		rsession.end();
	}
}

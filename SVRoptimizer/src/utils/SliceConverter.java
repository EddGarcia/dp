package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class SliceConverter {
	
	public void convertToSlice(double epsilonFix, String pathToFile) throws Exception{
		List<String> lines = new ArrayList<String>();
		PrintWriter writer = new PrintWriter("heatMapMatrix.txt","UTF-8");
		
		BufferedReader br = new BufferedReader(new FileReader(pathToFile));
		try {
			
			// Skip the header
		    String line = br.readLine();
		    while ((line = br.readLine()) != null) {
		        lines.add(line);
		    }
		    
		    String[] tokens = null;
		    double lastRecord = 0;
		    boolean firstRow = true;
		    for(String record : lines){
		    	tokens = record.split(",");
		    	
		    	
		    	if(Double.parseDouble(tokens[0]) == epsilonFix){
		    		if(lastRecord != Double.parseDouble(tokens[1])){
		    			lastRecord = Double.parseDouble(tokens[1]);
		    			writer.println();
		    		}
		    		else{
		    			if(!firstRow) writer.print(",");
		    			firstRow = false;
		    		}
		    		
		    		writer.print(Double.parseDouble(tokens[3]));
		    	}
		    }
		}catch(Exception e){
			e.printStackTrace();
		} finally {
		    br.close();
		    writer.close();
		}
	}
}

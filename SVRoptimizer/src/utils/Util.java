package utils;

public class Util {
	public static double arrayMin(double[] array){
		double min = Double.MAX_VALUE;
		for(double d : array){
			if(d < min) min = d;
		}
		return min;
	}
}
